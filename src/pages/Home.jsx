import { Flex } from '@chakra-ui/react';
import { useNavigate } from 'react-router-dom';
import TMButton from '../components/atoms/TMButton';

const Home = () => {
  const navigate = useNavigate();
  const redirectToUsuarie = () => navigate('/usuaries/welcome');
  const redirectToVoluntarie = () => navigate('/voluntaries');

  return (
    <Flex
      w="100%"
      h="100vh"
      direction="column"
      alignItems="center"
      justifyContent="center"
    >
      <TMButton
        bgColor="#5C186A"
        textColor="#FFFFFF"
        w="20rem"
        h="2.875rem"
        mb="2rem"
        onClick={redirectToUsuarie}
      >
        Plataforma usuaries
      </TMButton>

      <TMButton
        bgColor="#5C186A"
        textColor="#FFFFFF"
        w="20rem"
        h="2.875rem"
        mb="2rem"
        onClick={redirectToVoluntarie}
      >
        Plataforma voluntaries
      </TMButton>
    </Flex>
  );
};

export default Home;
