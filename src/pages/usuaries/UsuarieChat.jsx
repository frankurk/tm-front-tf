import { Flex, useBoolean, Box, Text } from '@chakra-ui/react';
import { useState, useContext } from 'react';
import UsuarieChatHeader from '../../components/molecules/UsuarieChatHeader';
import ReceivedMessage from '../../components/molecules/ReceivedMessage';
import SentMessage from '../../components/molecules/SentMessage';
import UsuarieChatBottom from '../../components/molecules/UsuarieChatBottom';
import { ref, onValue } from 'firebase/database';
import { database } from '../../firebaseConfig';
import { userAuthContext } from '../../context/UserAuthContext';

const UsuarieChat = () => {
  const [options, setOptions] = useBoolean();
  const [usuarieChats, setUsuarieChats] = useState([]);
  const { user } = useContext(userAuthContext);

  const dbChatRef = ref(database, `/chats/`);
  onValue(dbChatRef, (snapshot) => {
    const dataChats = snapshot.val();
    const sentMessages = dataChats && Object.values(dataChats);
    if (sentMessages && sentMessages.length !== usuarieChats.length) {
      setUsuarieChats(sentMessages);
    }
  });

  return (
    <Flex
      w="100%"
      flexDirection="column"
      h="100vh"
    >
      <UsuarieChatHeader onClick={setOptions.toggle} />
      {options ? (
        <Box
          m="0"
          h="50px"
          w="100%"
          bg="white"
          p="4"
        >
          <Text
            fontSize="14px"
            textDecoration="underline"
          >
            Revisar términos y condiciones
          </Text>
        </Box>
      ) : null}
      <Box
        w="100%"
        height="80%"
        backgroundColor="#e5e5e5"
        p="6"
        overflow="auto"
      >
        <ReceivedMessage content="¡Hola! Bienvenide a Hora Segura, un espacio seguro para conversar. Cuéntanos ¿en qué te podemos ayudar hoy? 🥰" />
        {usuarieChats.map((chat) =>
          user.uid === chat.message?.sender ? (
            <SentMessage
              content={chat.message?.content}
              key={chat.index}
            />
          ) : (
            <ReceivedMessage
              content={chat.message?.content}
              key={chat.index}
            />
          )
        )}
      </Box>
      <UsuarieChatBottom />
    </Flex>
  );
};

export default UsuarieChat;
