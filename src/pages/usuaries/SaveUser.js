import { set, ref, push } from 'firebase/database';
import { database } from '../../firebaseConfig';

const SaveUser = (id, name, age, comuna, gender, bioSex, sexualOrientation) => {
  const date = new Date();
  const dataUser = {
    userId: id,
    nombre: name,
    edad: age,
    region: '',
    comuna: comuna,
    identidadGenero: gender,
    sexoBiologico: bioSex,
    orientacionSexoafectiva: sexualOrientation,
    discapacidad: false,
    fechaIngreso: date.toLocaleDateString(),
  };
  const postUserRef = ref(database, '/usuaries/');
  const newUser = push(postUserRef);
  set(newUser, dataUser);
};
export default SaveUser;
