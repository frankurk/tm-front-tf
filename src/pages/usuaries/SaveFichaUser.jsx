import { set, ref, push } from 'firebase/database';
import { database } from '../../firebaseConfig';

const SaveFichaUser = (
  lgbtiq,
  osieg,
  trans,
  noBinario,
  nna,
  salidaDelCloset,
  acosooMaltrato,
  ciberbullying,
  sintomatologiaAnsiosa,
  comportamientoSuicida,
  sintomatologiaDepresiva,
  consumoDeSustancias,
  vulneraciondeDerechos,
  violenciaEnLaPareja,
  solicitaOtrasInformaciones,
  noCaracterizable,
  conversacionExitosa,
  caracterizacionOk,
  educacionoSaludSexual,
  derivacion,
  personaMigrante,
  rechazoFamiliar,
  entornoProtector,
  trAlimentario
) => {
  const dataFichaUser = {
    lgbtiq: lgbtiq,
    osieg: osieg,
    trans: trans,
    noBinario: noBinario,
    nna: nna,
    salidaDelCloset: salidaDelCloset,
    acosooMaltrato: acosooMaltrato,
    ciberbullying: ciberbullying,
    sintomatologiaAnsiosa: sintomatologiaAnsiosa,
    comportamientoSuicida: comportamientoSuicida,
    sintomatologiaDepresiva: sintomatologiaDepresiva,
    consumoDeSustancias: consumoDeSustancias,
    vulneraciondeDerechos: vulneraciondeDerechos,
    violenciaEnLaPareja: violenciaEnLaPareja,
    solicitaOtrasInformaciones: solicitaOtrasInformaciones,
    noCaracterizable: noCaracterizable,
    conversaciónExitosa: conversacionExitosa,
    caracterizacionOk: caracterizacionOk,
    educacionoSaludSexual: educacionoSaludSexual,
    derivacion: derivacion,
    personaMigrante: personaMigrante,
    rechazoFamiliar: rechazoFamiliar,
    entornoProtector: entornoProtector,
    trAlimentario: trAlimentario,
  };
  const postFichaUserRef = ref(database, '/fichaUsuaries/');
  const newFichaUser = push(postFichaUserRef);
  set(newFichaUser, dataFichaUser);
};

export default SaveFichaUser;
