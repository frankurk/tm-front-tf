import AuthTemplate from '../../components/templates/AuthTemplate';

const VoluntarieAuth = () => {
  return <AuthTemplate />;
};

export default VoluntarieAuth;
