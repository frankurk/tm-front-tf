import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

import Home from '../pages/Home';
import UsuarieWelcome from '../pages/usuaries/UsuarieWelcome';
import Index from '../pages/voluntaries/VoluntarieWelcome';
import UsuarieForm from '../pages/usuaries/UsuarieForm';
import VoluntariesChat from '../pages/voluntaries/VoluntarieChat';
import VoluntarieAuth from '../pages/voluntaries/VoluntarieAuth';
import UsuarieAuth from '../pages/usuaries/UsuarieAuth';
import UsuarieChat from '../pages/usuaries/UsuarieChat';
import { VoluntariesAuthProvider } from '../context/VoluntariesAuthContext';

const AppRouter = () => {
  return (
    <Router>
      <Routes>
        <Route
          exact
          path="/"
          element={<Home />}
        />
        <Route
          exact
          path="/usuaries/welcome"
          element={<UsuarieWelcome />}
        />
        <Route
          exact
          path="/usuaries/auth"
          element={<UsuarieAuth />}
        />
        <Route
          exact
          path="/usuaries/form"
          element={<UsuarieForm />}
        />
        <Route
          exact
          path="/usuaries/chat"
          element={<UsuarieChat />}
        />
        <Route
          exact
          path="/voluntaries"
          element={<Index />}
        />
        <Route
          path="/voluntaries/auth"
          element={<VoluntarieAuth />}
        />
        <Route
          exact
          path="/voluntaries/chat"
          element={
            <VoluntariesAuthProvider>
              {' '}
              <VoluntariesChat />{' '}
            </VoluntariesAuthProvider>
          }
        />
      </Routes>
    </Router>
  );
};

export { AppRouter as default };
