import { createContext, useContext, useReducer } from 'react';
import initialState from './initialState';
import reducer from './reducers';

const DispatchContext = createContext((params) => params);
const StateContext = createContext(initialState);

const Provider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <DispatchContext.Provider value={dispatch}>
      <StateContext.Provider value={state}>{children}</StateContext.Provider>
    </DispatchContext.Provider>
  );
};

const useState = () => useContext(StateContext);
const useDispatch = () => useContext(DispatchContext);

export { Provider as default, useState, useDispatch };
