import { HStack, Input, Box } from '@chakra-ui/react';
import EmojiPicker from '@emoji-mart/react/react';
import { push, set, ref } from 'firebase/database';
import { useState, useContext } from 'react';
import { userAuthContext } from '../../context/UserAuthContext';
import TMImage from '../atoms/TMImage';
import TMButton from '../atoms/TMButton';
import { database } from '../../firebaseConfig';

const VoluntarieChatBottom = () => {
  const { user } = useContext(userAuthContext);
  const [inputVoluntarie, setInputVoluntarie] = useState('');
  const date = new Date();
  const [showEmojis, setShowEmojis] = useState(false);

  const message = {
    sender: user.uid,
    idUsuarie: '9pzjBRIX3aX5kQoxGy7y5ZTBVSG3',
    idVoluntarie: user.uid,
    day: date.toLocaleDateString(),
    hour: date.toLocaleTimeString(),
    content: inputVoluntarie,
  };

  const data = (message) => {
    const postListRef = ref(database, `/chats/`);
    const newMessage = push(postListRef);
    set(newMessage, {
      message,
    });
  };

  const valueInputVoluntarie = (e) => {
    setInputVoluntarie(e.target.value);
  };

  const addEmoji = (e) => {
    const symbol = e.unified.split('-');
    const codesArray = [];
    symbol.forEach((element) => codesArray.push('0x' + element));
    const emoji = String.fromCodePoint(...codesArray);
    setInputVoluntarie(inputVoluntarie + emoji);
  };

  const handleEmoji = () => {
    setShowEmojis(!showEmojis);
  };

  const handleMessageSubmit = () => {
    setInputVoluntarie('');
    data(message);
  };

  const handleEnter = (e) => {
    if (e.key === 'Enter') {
      handleMessageSubmit();
    }
  };

  return (
    <HStack
      justifyContent="space-between"
      w="100%"
      h="10%"
      px="5"
      py="6"
      bg="white"
      borderLeft="1px"
      borderRight="1px"
      borderColor="#BEBEBE"
    >
      <TMButton
        variant={'ghost'}
        onClick={handleEmoji}
      >
        <TMImage
          src={'/icons/emoji-keyboard.svg'}
          boxSize={'2rem'}
        />
      </TMButton>
      {showEmojis && (
        <Box
          position="fixed"
          bottom="90px"
          z-index="1000"
        >
          <EmojiPicker onEmojiSelect={addEmoji} />
        </Box>
      )}
      <Input
        w="90%"
        borderColor="#8E8E8E"
        borderRadius="9px"
        value={inputVoluntarie}
        onChange={valueInputVoluntarie}
        onKeyDown={(e) => handleEnter(e)}
      />
      <TMButton
        variant={'ghost'}
        backgroundColor={'#5C186A'}
        borderRadius={'50%'}
        w="10"
        h="10"
        p="0"
        onClick={handleMessageSubmit}
        disabled={!inputVoluntarie}
      >
        <TMImage
          src={'/icons/send.svg'}
          h="4"
          w="5"
        />
      </TMButton>
    </HStack>
  );
};

export default VoluntarieChatBottom;
