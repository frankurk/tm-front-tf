import { HStack, Button } from '@chakra-ui/react';

import TMImage from '../atoms/TMImage';

const UsuarieChatHeader = ({ onClick }) => {
  return (
    <HStack
      wrap="wrap"
      justifyContent="space-between"
      bg="white"
      h="10%"
      w="100%"
      py="6"
      px="7"
      boxShadow="0px 4px 16px rgba(0, 0, 0, 0.1);"
    >
      <TMImage
        h="10"
        src={'/images/logo-todo-mejora.svg'}
        alt={'Logo de Todo Mejora'}
      />
      <Button variant="ghost">
        <TMImage
          src={'/icons/options.svg'}
          onClick={onClick}
        />
      </Button>
    </HStack>
  );
};

export default UsuarieChatHeader;
