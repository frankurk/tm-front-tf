import {
  fichaButtonProps,
  fichaButtonPropsClicked,
} from '../../themes/todomejora';
import TMButton from '../../components/atoms/TMButton';
import { useUserAuth } from '../../context/UserAuthContext';
import { Center, Container, useControllableState } from '@chakra-ui/react';
import SaveFichaUser from '../../pages/usuaries/SaveFichaUser';

import {
  Box,
  Text,
  Flex,
  Tabs,
  TabList,
  TabPanels,
  Tab,
  TabPanel,
  Divider,
  Stack,
  Button,
  VStack,
  Textarea,
} from '@chakra-ui/react';

const FichaUsuarie = () => {
  const user = useUserAuth();
  const handleClick = () => {
    SaveFichaUser(
      lgbtiq,
      osieg,
      trans,
      noBinario,
      nna,
      salidaDelCloset,
      acosooMaltrato,
      ciberbullying,
      sintomatologiaAnsiosa,
      comportamientoSuicida,
      sintomatologiaDepresiva,
      consumoDeSustancias,
      vulneraciondeDerechos,
      violenciaEnLaPareja,
      solicitaOtrasInformaciones,
      noCaracterizable,
      conversacionExitosa,
      caracterizacionOk,
      educacionoSaludSexual,
      derivacion,
      personaMigrante,
      rechazoFamiliar,
      entornoProtector,
      trAlimentario
    );
    console.log(SaveFichaUser);
    console.log(user);
  };

  const [lgbtiq, setLgbtiq] = useControllableState(false);
  const [osieg, setOsieg] = useControllableState(false);
  const [trans, setTrans] = useControllableState(false);
  const [noBinario, setNoBinario] = useControllableState(false);
  const [nna, setNna] = useControllableState(false);
  const [sexualOrientation, setSexualOrientation] = useControllableState(false);
  const [salidaDelCloset, setSalidaDelCloset] = useControllableState(false);
  const [acosooMaltrato, setAcosooMaltrato] = useControllableState(false);
  const [ciberbullying, setCiberbullying] = useControllableState(false);
  const [sintomatologiaAnsiosa, setSintomatologiaAnsiosa] =
    useControllableState(false);
  const [comportamientoSuicida, setComportamientoSuicida] =
    useControllableState(false);
  const [sintomatologiaDepresiva, setSintomatologiaDepresiva] =
    useControllableState(false);
  const [consumoDeSustancias, setConsumoDeSustancias] =
    useControllableState(false);
  const [vulneraciondeDerechos, setVulneraciondeDerechos] =
    useControllableState(false);
  const [violenciaEnLaPareja, setViolenciaEnLaPareja] =
    useControllableState(false);
  const [solicitaOtrasInformaciones, setSolicitaOtrasInformaciones] =
    useControllableState(false);
  const [noCaracterizable, setNoCaracterizable] = useControllableState(false);
  const [conversacionExitosa, setConversacionExitosa] =
    useControllableState(false);
  const [caracterizacionOk, setCaracterizacionOk] = useControllableState(false);
  const [educacionoSaludSexual, setEducacionoSaludSexual] =
    useControllableState(false);
  const [derivacion, setDerivacion] = useControllableState(false);
  const [personaMigrante, setPersonaMigrante] = useControllableState(false);
  const [rechazoFamiliar, setRechazoFamiliar] = useControllableState(false);
  const [entornoProtector, setEntornoProtector] = useControllableState(false);
  const [trAlimentario, setTrAlimentario] = useControllableState(false);

  const clickedProps = (hook, value) => {
    if (hook === value) {
      return fichaButtonPropsClicked;
    } else {
      return fichaButtonProps;
    }
  };

  return (
    <Box
      borderLeft="1px"
      borderColor="#BEBEBE"
      w="25%"
      h="100%"
    >
      <Flex
        h="8%"
        textAlign="start"
        ml="6"
        alignItems="center"
      >
        <Text fontWeight="800">Ficha Usuarie</Text>
      </Flex>
      <Container
        h="90%"
        borderTop="1px"
        borderColor="#BEBEBE"
      >
        <Box
          display="flex"
          p="2"
          m="2"
          flexDirection="row"
          h="30%"
          justifyContent="space-between"
        >
          <VStack
            h="100%"
            alignItems="flex-start"
          >
            <Text
              fontSize="xs"
              fontWeight="bold"
            >
              {' '}
              Nombre/Apodo: {''}
              <Text fontWeight="normal">Cecilia</Text>
            </Text>
            <Text
              fontSize="xs"
              fontWeight="bold"
            >
              {' '}
              Comuna: {''}
              <Text fontWeight="normal">La Florida</Text>
            </Text>
            <Text
              fontSize="xs"
              fontWeight="bold"
            >
              {' '}
              Sexo Biológico: {''}
              <Text fontWeight="normal">Hembra</Text>
            </Text>
          </VStack>
          <VStack
            h="100%"
            alignItems="flex-start"
          >
            <Text
              fontSize="xs"
              fontWeight="bold"
            >
              {' '}
              Edad: {''}
              <Text fontWeight="normal">15</Text>
            </Text>
            <Text
              fontSize="xs"
              fontWeight="bold"
            >
              {' '}
              Identidad de Género: {''}
              <Text fontWeight="normal">Estoy descubriéndolo</Text>
            </Text>
            <Text
              fontSize="xs"
              fontWeight="bold"
            >
              {' '}
              Orientación Sexoafectiva: {''}
              <Text fontWeight="normal">Estoy descubriéndolo</Text>
            </Text>
          </VStack>
        </Box>
        <Divider />
        <Stack h="55%">
          <Tabs
            variant="enclosed"
            borderColor="#5C186A"
            color="#5C186A"
            onFocusCapture="#5C186A"
          >
            <TabList>
              <Tab>Atributos</Tab>
              <Tab>Tags</Tab>
            </TabList>
            <TabPanels h="250">
              <TabPanel>
                <Textarea
                  border="1px"
                  borderColor="#BEBEBE"
                  h="200"
                  placeholder="Resumen de tu última conversación"
                />
                <Center>
                  <TMButton
                    bgColor="#5C186A"
                    textColor="#FFFFFF"
                    mt="10"
                  >
                    Guardar Resumen
                  </TMButton>
                </Center>
              </TabPanel>
              <TabPanel h="100%">
                <Flex
                  flexDirection="row"
                  h="100%"
                  wrap="wrap"
                  overflow="scroll"
                >
                  <Button
                    m="4px"
                    {...clickedProps(lgbtiq, true)}
                    onClick={() => {
                      setLgbtiq('true');
                    }}
                  >
                    LGBTIQ+
                  </Button>
                  <Button
                    m="4px"
                    {...clickedProps(osieg, 'true')}
                    onClick={() => {
                      setOsieg('true');
                    }}
                  >
                    OSIEG
                  </Button>
                  <Button
                    m="4px"
                    {...clickedProps(trans, 'true')}
                    onClick={() => {
                      setTrans('true');
                    }}
                  >
                    Trans
                  </Button>
                  <Button
                    m="4px"
                    {...clickedProps(noBinario, 'true')}
                    onClick={() => {
                      setNoBinario('true');
                    }}
                  >
                    No Binario
                  </Button>
                  <Button
                    m="4px"
                    {...clickedProps(nna, 'true')}
                    onClick={() => {
                      setNna('true');
                    }}
                  >
                    NNA
                  </Button>
                  <Button
                    m="4px"
                    {...clickedProps(acosooMaltrato, 'true')}
                    onClick={() => {
                      setAcosooMaltrato('true');
                    }}
                  >
                    Acoso o Maltrato
                  </Button>
                  <Button
                    m="4px"
                    {...clickedProps(salidaDelCloset, 'true')}
                    onClick={() => {
                      setSalidaDelCloset('true');
                    }}
                  >
                    Salida del clóset
                  </Button>

                  <Button
                    m="4px"
                    {...clickedProps(ciberbullying, 'false')}
                    onClick={() => {
                      setCiberbullying('true');
                    }}
                  >
                    ciberbullying
                  </Button>
                  <Button
                    m="4px"
                    {...clickedProps(sintomatologiaAnsiosa, 'false')}
                    onClick={() => {
                      setSintomatologiaAnsiosa('true');
                    }}
                  >
                    Sintomatología Ansiosa
                  </Button>

                  <Button
                    m="4px"
                    {...clickedProps(comportamientoSuicida, 'true')}
                    onClick={() => {
                      setComportamientoSuicida('true');
                    }}
                  >
                    Comportamiento Suicida
                  </Button>
                  <Button
                    m="4px"
                    {...clickedProps(sintomatologiaDepresiva, 'true')}
                    onClick={() => {
                      sintomatologiaDepresiva('true');
                    }}
                  >
                    NNA
                  </Button>
                  <Button
                    m="4px"
                    {...clickedProps(consumoDeSustancias, 'true')}
                    onClick={() => {
                      setConsumoDeSustancias('true');
                    }}
                  >
                    Consumo de Sustancias
                  </Button>

                  <Button
                    m="4px"
                    {...clickedProps(violenciaEnLaPareja, 'true')}
                    onClick={() => {
                      setViolenciaEnLaPareja('true');
                    }}
                  >
                    violencia En LaPareja
                  </Button>
                  <Button
                    m="4px"
                    {...clickedProps(solicitaOtrasInformaciones, 'true')}
                    onClick={() => {
                      setSolicitaOtrasInformaciones('true');
                    }}
                  >
                    Solicita otras Informaciones
                  </Button>
                  <Button
                    m="4px"
                    {...clickedProps(conversacionExitosa, 'true')}
                    onClick={() => {
                      setConversacionExitosa('true');
                    }}
                  >
                    Solicita otras Informaciones
                  </Button>

                  <Button
                    m="4px"
                    {...clickedProps(noCaracterizable, 'true')}
                    onClick={() => {
                      setNoCaracterizable('true');
                    }}
                  >
                    No Caracterizable
                  </Button>

                  <Button
                    m="4px"
                    {...clickedProps(caracterizacionOk, 'true')}
                    onClick={() => {
                      setCaracterizacionOk('true');
                    }}
                  >
                    Caracterización OK
                  </Button>
                  <Button
                    m="4px"
                    {...clickedProps(educacionoSaludSexual, false)}
                    onClick={() => {
                      setEducacionoSaludSexual('true');
                    }}
                  >
                    Educación o salud sexual
                  </Button>
                  <Button
                    m="4px"
                    {...clickedProps(derivacion, 'false')}
                    onClick={() => {
                      setDerivacion('true');
                    }}
                  >
                    Derivación
                  </Button>
                  <Button
                    m="4px"
                    {...clickedProps(personaMigrante, 'false')}
                    onClick={() => {
                      setPersonaMigrante('true');
                    }}
                  >
                    Persona Migrante
                  </Button>

                  <Button
                    m="4px"
                    {...clickedProps(rechazoFamiliar, 'false')}
                    onClick={() => {
                      setRechazoFamiliar('true');
                    }}
                  >
                    Rechazo familiar
                  </Button>
                  <Button
                    m="4px"
                    {...clickedProps(entornoProtector, 'false')}
                    onClick={() => {
                      setEntornoProtector('true');
                    }}
                  >
                    NNA
                  </Button>
                  <Button
                    m="4px"
                    {...clickedProps(trAlimentario, 'true')}
                    onClick={() => {
                      setTrAlimentario('true');
                    }}
                  >
                    Consumo de Sustancias
                  </Button>
                  <Button
                    m="4px"
                    {...clickedProps(vulneraciondeDerechos, 'true')}
                    onClick={() => {
                      setVulneraciondeDerechos('true');
                    }}
                  >
                    Vulneración Derechos
                  </Button>
                  <Button
                    m="4px"
                    {...clickedProps(sexualOrientation, 'true')}
                    onClick={() => {
                      setSexualOrientation('true');
                    }}
                  >
                    Orientacion Sexual
                  </Button>
                  <Button
                    m="4px"
                    {...clickedProps(sintomatologiaDepresiva, 'true')}
                    onClick={() => {
                      setSintomatologiaDepresiva('true');
                    }}
                  >
                    sintomatologiaDepresiva
                  </Button>
                </Flex>
                <Center>
                  <TMButton
                    bgColor="#5C186A"
                    textColor="#FFFFFF"
                    mt="10"
                    // onClick={handleClick}
                  >
                    Enviar
                  </TMButton>
                </Center>
              </TabPanel>
            </TabPanels>
          </Tabs>
        </Stack>
      </Container>
    </Box>
  );
};

export default FichaUsuarie;
