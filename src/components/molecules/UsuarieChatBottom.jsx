import { set, ref, push } from 'firebase/database';
import TMImage from '../atoms/TMImage';
import TMButton from '../atoms/TMButton';
import { useState } from 'react';
import { HStack, Input, Box } from '@chakra-ui/react';
import EmojiPicker from '@emoji-mart/react/react';

import { database } from '../../firebaseConfig';
import { useUserAuth } from '../../context/UserAuthContext';

const UsuarieChatBottom = () => {
  const date = new Date();
  const [input, setInput] = useState('');
  const { user } = useUserAuth();
  const [showEmojis, setShowEmojis] = useState(false);

  const message = {
    sender: user.uid,
    idUsuarie: user.uid,
    idVoluntarie: 't0OCdXkoAxb8nZXlGnWdFS7CZFN2',
    day: date.toLocaleDateString(),
    hour: date.toLocaleTimeString(),
    content: input,
  };

  const data = (message) => {
    const postListRef = ref(database, `/chats/`);
    const newMessage = push(postListRef);
    set(newMessage, {
      message,
    });
  };

  const valueInput = (e) => {
    setInput(e.target.value);
  };
  const addEmoji = (e) => {
    const symbol = e.unified.split('-');
    const codesArray = [];
    symbol.forEach((element) => codesArray.push('0x' + element));
    const emoji = String.fromCodePoint(...codesArray);
    setInput(input + emoji);
  };

  const handleEmoji = () => {
    setShowEmojis(!showEmojis);
  };

  const handleMessageSubmit = () => {
    setInput('');
    data(message);
  };

  const handleEnter = (e) => {
    if (e.key === 'Enter') {
      handleMessageSubmit();
    }
  };

  return (
    <HStack
      justifyContent="space-between"
      w="100%"
      h="10%"
      px="5"
      py="6"
      bg="white"
      boxShadow="0px -3px 16px rgba(0, 0, 0, 0.1);"
    >
      <TMButton
        variant={'ghost'}
        onClick={handleEmoji}
      >
        <TMImage
          src={'/icons/emoji-keyboard.svg'}
          boxSize={'2rem'}
        />
      </TMButton>
      {showEmojis && (
        <Box
          position="fixed"
          bottom="85px"
          z-index="1000"
        >
          <EmojiPicker onEmojiSelect={addEmoji} />
        </Box>
      )}
      <Input
        w="90%"
        borderColor="#8E8E8E"
        borderRadius="9px"
        value={input}
        onChange={valueInput}
        onKeyDown={(e) => handleEnter(e)}
      />
      <TMButton
        variant={'ghost'}
        backgroundColor={'#5C186A'}
        borderRadius={'50%'}
        w="10"
        h="10"
        p="0"
        onClick={handleMessageSubmit}
        disabled={!input}
      >
        <TMImage
          src={'/icons/send.svg'}
          h="4"
          w="5"
        />
      </TMButton>
    </HStack>
  );
};

export default UsuarieChatBottom;
