import { Box, Text } from '@chakra-ui/react';
import TMImage from '../atoms/TMImage';

const ChatNewMsj = () => {
  return (
    <>
      <Box
        display="flex"
        flexDirection="column"
        h="20%"
        w="100%"
        borderBottom="1px"
        borderColor="#BEBEBE"
      >
        <Box
          display="flex"
          alignItems="center"
          mt="7"
          h="20%"
        >
          <TMImage
            src={'/icons/ellipse.svg'}
            m="4"
            w="3"
          />{' '}
          <Text>+56 9 12345678</Text>
        </Box>
        <Box
          display="flex"
          textAlign="start"
          alignItems="center"
          ml="12"
          fontSize="md"
        >
          <Text>New message</Text>
        </Box>
      </Box>
    </>
  );
};

export default ChatNewMsj;
