import { Box, Flex, Text } from '@chakra-ui/react';
import { useRef, useEffect } from 'react';
import '../../App.css';

const ReceivedMessage = ({ content }) => {
  const messageRef = useRef();

  useEffect(() => {
    if (messageRef) messageRef.current.scrollIntoView({ behavior: 'smooth' });
  }, []);

  return (
    <Flex
      alignItems="start"
      direction="column"
      maxW="md"
    >
      <Box
        mt="0.75rem"
        backgroundColor="#ffffff"
        borderRadius="0.562rem"
        alignItems="end"
        className={'receivedMessage'}
      >
        <Text
          color="#333333"
          fontSize="0.875rem"
          fontWeight="400"
          m="1rem"
          ref={messageRef}
        >
          {content}
        </Text>
      </Box>
    </Flex>
  );
};

export default ReceivedMessage;
