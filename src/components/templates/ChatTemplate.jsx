import { Box, Flex } from '@chakra-ui/react';
import TestProvider from '../../store/test/context';
import VoluntarieChatHeader from '../molecules/VoluntarieChatHeader';
import ChatList from '../organisms/voluntaries/ChatList';
import ChatViewFull from '../organisms/voluntaries/ChatViewFull';
import FichaUsuarie from '../molecules/FichaUsuarie';

const ChatTemplate = () => {
  return (
    <TestProvider>
      <Flex
        flexDirection="column"
        justifyContent="center"
        alignItems="center"
        w="100vw"
        h="100vh"
      >
        <Box
          border="1px"
          borderColor="#BEBEBE"
          h="95%"
          w="95%"
        >
          <VoluntarieChatHeader />
          <Box
            display="flex"
            w="100%"
            h="90%"
            borderColor="neutral.gray"
          >
            <ChatList />
            <ChatViewFull />
            <FichaUsuarie />
          </Box>
        </Box>
      </Flex>
    </TestProvider>
  );
};

export default ChatTemplate;
