import {
  Box,
  Button,
  Input,
  FormControl,
  FormLabel,
  Heading,
  Flex,
} from '@chakra-ui/react';
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { auth } from '../../firebaseConfig';
import TMImage from '../atoms/TMImage';

const AuthTemplate = () => {
  const [ready, setReady] = useState(false);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const emailValidator = /.+@.+\..+/;
  const navigate = useNavigate();

  const handleEmailInput = (e) =>
    emailValidator.test(e.target.value)
      ? setEmail(e.target.value)
      : setEmail('');

  const validateInputValues = (e) => {
    const password = e.target.value;

    if (password === '') setReady(false);
    if (email && password) {
      setPassword(password);
      setReady(true);
    }
  };

  const handleSubmit = () => {
    auth.signInWithEmailAndPassword(email, password).then((cred) => {
      // eslint-disable-next-line no-undef
      localStorage.setItem('voluntarie', JSON.stringify(cred.user));
      navigate('/voluntaries/chat', { replace: true });
    });
  };

  return (
    <Box
      display="flex"
      flexDirection="column"
      alignItems="center"
      justifyContent="center"
      mt="4rem"
    >
      <TMImage
        src={'/images/logo-todo-mejora.svg'}
        w={200}
        mb={5}
        mt={5}
      />
      <Flex
        justifyContent="center"
        alignItems="center"
        flexDirection="column"
        w="25%"
        minW="22rem"
        boxShadow="2xl"
        p="6"
      >
        <FormControl>
          <Heading
            size={6}
            color="neutral.black"
            mt={5}
            mb={5}
            fontWeight="600"
          >
            Inicia sesión con tu cuente Todo Mejora.
          </Heading>
          <FormLabel fontWeight="700">Mail de Usuario</FormLabel>
          <Input
            mb={10}
            type="email"
            fontWeight="400"
            lineHeight="22px"
            id="email"
            onBlur={handleEmailInput}
          />
          <FormLabel fontWeight="700">Contraseña</FormLabel>
          <Input
            mb={10}
            type="password"
            id="password"
            onChange={validateInputValues}
            onBlur={validateInputValues}
          />
        </FormControl>
        {ready ? (
          <Button
            align="center"
            w="20rem"
            h="2.875rem"
            mt="4"
            bg="#5C186A"
            textColor="#FFFFFF"
            onClick={handleSubmit}
          >
            Entrar
          </Button>
        ) : (
          <Button
            align="center"
            w="20rem"
            h="2.875rem"
            mt="4"
            bg="#BEBEBE"
            textColor="#FFFFFF"
            onClick={handleSubmit}
          >
            Entrar
          </Button>
        )}

        <Button
          color="#5C186A"
          variant="link"
          mt={5}
          mb={5}
        >
          Olvidé mi Contraseña
        </Button>
      </Flex>
    </Box>
  );
};

export default AuthTemplate;
