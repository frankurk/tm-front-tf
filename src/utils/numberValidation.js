export const isNumeric = (number) => {
  return !isNaN(number);
};
